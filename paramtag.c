/* externa */
undefined4 Get_Tag_Param(void *param_1)

{
  int __fd;
  ssize_t sVar1;
  undefined4 uVar2;
  
  __fd = open((char *)0x1700,0x102);
  if (__fd < 0) {
    printf((char *)0x1714,0x1a60,0x8b,0x1700);
    uVar2 = 0xfffffff7;
  }
  else {
    sVar1 = read(__fd,param_1,0x1000);
    uVar2 = 0;
    if (sVar1 < 0) {
      printf((char *)0x173c,0x1a60,0x94,0x1700,sVar1);
      uVar2 = 0xffffffff;
    }
    close(__fd);
  }
  return uVar2;
}




uint crc32.constprop.3(byte *param_1,uint param_2)

{
  uint uVar1;
  byte *pbVar2;
  uint uVar3;
  
  uVar1 = 0xffffffff;
  pbVar2 = param_1;
  uVar3 = param_2;
  while (7 < uVar3) {
    uVar3 = uVar3 - 8;
    uVar1 = *(uint *)(((*pbVar2 ^ uVar1) & 0xff) * 4 + 0x1a70) ^ uVar1 >> 8;
    uVar1 = *(uint *)(((uVar1 ^ pbVar2[1]) & 0xff) * 4 + 0x1a70) ^ uVar1 >> 8;
    uVar1 = *(uint *)(((uVar1 ^ pbVar2[2]) & 0xff) * 4 + 0x1a70) ^ uVar1 >> 8;
    uVar1 = *(uint *)(((uVar1 ^ pbVar2[3]) & 0xff) * 4 + 0x1a70) ^ uVar1 >> 8;
    uVar1 = *(uint *)(((uVar1 ^ pbVar2[4]) & 0xff) * 4 + 0x1a70) ^ uVar1 >> 8;
    uVar1 = *(uint *)(((uVar1 ^ pbVar2[5]) & 0xff) * 4 + 0x1a70) ^ uVar1 >> 8;
    uVar1 = *(uint *)(((uVar1 ^ pbVar2[6]) & 0xff) * 4 + 0x1a70) ^ uVar1 >> 8;
    uVar1 = *(uint *)(((uVar1 ^ pbVar2[7]) & 0xff) * 4 + 0x1a70) ^ uVar1 >> 8;
    pbVar2 = pbVar2 + 8;
  }
  uVar3 = param_2 & 7;
  param_1 = param_1 + (param_2 & 0xfffffff8);
  while (uVar3 != 0) {
    uVar3 = uVar3 - 1;
    uVar1 = *(uint *)(((*param_1 ^ uVar1) & 0xff) * 4 + 0x1a70) ^ uVar1 >> 8;
    param_1 = param_1 + 1;
  }
  return ~uVar1;
}



/* externa */
undefined4 ReadParamTag(char *param_1)

{
  int iVar1;
  char *__format;
  undefined4 uVar2;
  
  iVar1 = Get_Tag_Param();
  if (iVar1 == 0) {
    iVar1 = strncmp(param_1,(char *)0x17e0,4);
    if (iVar1 != 0) {
      return 0xfffffffe;
    }
    iVar1 = crc32.constprop.3(param_1 + 0x10,*(undefined4 *)(param_1 + 0x10));
    if (*(int *)(param_1 + 0xc) == iVar1) {
      return 0;
    }
    __format = (char *)0x17e8;
    uVar2 = 0xce;
  }
  else {
    __format = (char *)0x17b4;
    uVar2 = 0xc4;
  }
  printf(__format,0x1a50,uVar2);
  return 0xfffffffe;
}

/*externa*/
int GetTagParam(uint param_1,void *param_2,uint param_3,size_t *param_4)

{
  void *__s;
  uint __n;
  uint uVar1;
  ushort *puVar2;
  ushort *puVar3;
  int iVar4;
  
  param_1 = param_1 & 0xffff;
  if (((param_2 == (void *)0x0) || (param_4 == (size_t *)0x0)) || (param_1 == 0)) {
    printf((char *)0x1814,0x1e70,0xea,param_1,param_2,param_4);
    iVar4 = -9;
  }
  else {
    __s = malloc(0x1000);
    if (__s == (void *)0x0) {
      printf((char *)0x1854,0x1e70,0xf1);
      iVar4 = -8;
    }
    else {
      memset(__s,0,0x1000);
      iVar4 = ReadParamTag(__s);
      if (iVar4 == 0) {
        puVar2 = (ushort *)((int)__s + 0x14);
        do {
          puVar3 = puVar2;
          __n = (uint)puVar3[2];
          if (__n == 0) {
LAB_00400c24:
            iVar4 = -7;
            goto LAB_00400c28;
          }
          uVar1 = *(int *)((int)__s + 0x10) + 0x10;
          puVar2 = (ushort *)((int)puVar3 + __n + 9 & 0xfffffffc);
          if (0x1000 < uVar1) {
            uVar1 = 0x1000;
          }
          if ((int)((int)__s + (uVar1 - (int)puVar2)) < 0) goto LAB_00400c24;
        } while (*puVar3 != param_1);
        if (param_3 < __n) {
          __n = param_3;
        }
        *param_4 = __n;
        memcpy(param_2,puVar3 + 3,__n);
      }
LAB_00400c28:
      free(__s);
    }
  }
  return iVar4;
}



ejemplo de uso 

int get_falsify_defend_mode(uint *param_1)

{
  int iVar1;
  uint uVar2;
  undefined4 local_40;
  char acStack60 [40];
  
  memset(acStack60,0,0x20);
  local_40 = 0;
  if (param_1 == (uint *)0x0) {
    ProcUserLog("pon_dev_mgr.c",0x198,"get_falsify_defend_mode",4,0,
                "get_falsify_defend_mode failed, Param is NULL!");
  }
  else {
    iVar1 = GetTagParam(0x896,acStack60,0x20,&local_40);
    if (iVar1 != 0) {
      ProcUserLog("pon_dev_mgr.c",0x1a9,"get_falsify_defend_mode",4,0,
                  "call  GetTagParam failed!!!!\n");
      return iVar1;
    }
    uVar2 = atoi(acStack60);
    if (uVar2 < 3) {
      *param_1 = uVar2;
      ProcUserLog("pon_dev_mgr.c",0x1ad,"get_falsify_defend_mode",7,0,"FALSFYMode is %d",uVar2);
      return 0;
    }
    ProcUserLog("pon_dev_mgr.c",0x1a2,"get_falsify_defend_mode",4,0,"FALSFYMode illeagal: %d",uVar2)
    ;
  }
  return -2;
}




