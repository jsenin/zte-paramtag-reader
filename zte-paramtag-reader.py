def read_binary(filename):
    content = None
    with open(filename, 'rb') as f:
        content = f.read()
    return content

def has_header(paramtag):
    return paramtag[0:8] == 'TAGH0201'

paramtag = read_binary('paramtag')
if not has_header(paramtag):
    print("Invalid header")
